package ru.averin.chat.server;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Input queue of a {@link ReadableByteChannel} instance
 */
@Slf4j
public class RBCInputQueue implements InputQueue<ReadableByteChannel> {
    // methods and fields are not synchronized because they are used only by one thread
    // of ConnectionHandler
    private final Queue<byte[]> queue = new LinkedList<>();
    private ByteBuffer buffer = ByteBuffer.allocate(1024);
    private byte[] currentMessage = new byte[0]; //total current message

    @Override
    public int fillFrom(ReadableByteChannel channel) throws IOException {
        int totalBytes = 0;
        int bytes;
        int cr, lf;
        while ((bytes = channel.read(buffer)) > 0) {
            byte[] array = buffer.array();
            do {
                //find a line separator - CR, LF or CR+LF
                cr = -1; lf = -1;
                for (int i = 0; i < buffer.position(); i++) {
                    if (array[i] == '\r') {
                        cr = i;
                        if (i + 1 < buffer.position() && array[i + 1] == '\n') {
                            lf = i + 1;
                        }
                        break;
                    }
                    if (array[i] == '\n') {
                        lf = i;
                        break;
                    }
                }
                // if it founded - cut off a piece from the head to its position and add to
                // the total currentMessage, then add the currentMessage in a queue, else
                // add all content of the buffer to the currentMessage and continue reading
                if (cr != -1 || lf != -1) {
                    int messageEnd = cr != -1 ? (lf != -1 ? Math.min(cr, lf) : cr) : lf;
                    byte[] messageTail = ArrayUtils.subarray(buffer.array(), 0, messageEnd);
                    if (currentMessage.length == 0 && messageTail.length == 0) {
                        throw new RuntimeException("message.length == 0");
                    }
                    currentMessage = ArrayUtils.addAll(currentMessage, messageTail);
                    queue.offer(currentMessage);
                    currentMessage = new byte[0];
                    // if separator is not in the end of read array - change its position for a next message
                    int nextMessageStart = Math.max(cr, lf) + 1;
                    if (nextMessageStart < buffer.position()) {
                        int position = buffer.position();
                        buffer.position(nextMessageStart);// first char of next message
                        buffer.compact(); //replace new message to the head of buffer
                        buffer.position(position - nextMessageStart); //first char after new message
                    } else {
                        // there are not bytes of a next message
                        buffer.clear();
                    }
                } else {
                    // if no free space in the buffer - add all read bytes to the total message
                    if (!buffer.hasRemaining()) {
                        currentMessage = ArrayUtils.addAll(currentMessage, ArrayUtils.subarray(buffer.array(), 0, buffer.position()));
                        buffer.clear();
                    }
                }
            } while (cr != -1 || lf != -1);
            totalBytes += bytes;
        }
        return totalBytes;
    }

    @Override
    public byte[] getNextMessage() {
        return queue.poll();
    }
}
