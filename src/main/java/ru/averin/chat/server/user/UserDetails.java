package ru.averin.chat.server.user;

public interface UserDetails {
    String getName();
    void setName(String name);
}
