package ru.averin.chat.server;

/**
 * Handle of a new connection
 */
@FunctionalInterface
public interface ConnectionAcceptHandler<T> {
    void register(T connection);
}
