package ru.averin.chat.server;

import java.io.IOException;

public interface InputQueue<T> {
    /**
     * Fills queue from the source. Reads bytes and splits them to the messages by CR, LF or CR+LF
     * @return The number of bytes read, possibly zero, or -1 if the source has reached end-of-stream
     */
    int fillFrom (T source) throws IOException;
    /**
     * Returns and removes the first message from the queue, of returns {@code null} if the queue is empty
     */
    byte[] getNextMessage();
}
