package ru.averin.chat.server.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
@ConfigurationProperties(prefix = "server")
@Getter @Setter
public class ServerConfig {
    String host;
    Integer port;
    Integer messagesCount;
    Integer botsCount;
    Integer workersCount;

    @Bean
    public ExecutorService executorService(){
        return Executors.newCachedThreadPool();
    }
}
