package ru.averin.chat.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Output queue of a {@link WritableByteChannel} instance
 */
@Slf4j
public class WBCOutputQueue implements OutputQueue<WritableByteChannel> {

    private static final byte[] lineSeparator = System.lineSeparator().getBytes();
    private final Queue<byte[]> queue = new ConcurrentLinkedQueue<>();
    private ByteBuffer current = null;  //current message

    @Override
    public int drainTo(WritableByteChannel channel) throws IOException {
        int bytesWritten = 0;
        while (true) {
            if (current == null) {
                if (queue.size() == 0) {
                    break;
                }
                current = ByteBuffer.wrap(queue.poll());
            }

            int rc = channel.write(current);
            bytesWritten += rc;
            //if full message was written - add the line separator
            if (!current.hasRemaining()) {
                current = null;
                int lf = channel.write(ByteBuffer.wrap(lineSeparator));
                rc += lf;
                bytesWritten += lf;
                // TODO: 12.07.17 potentially lf may be 0 (failed to write the line separator) - need to handle such a situation
            }
            if (rc == 0) {
                break;
            }
        }
        if (queue.size() > 100){
            log.warn("Client output queue is large. Size: " + queue.size());
        }
        return bytesWritten;
    }

    @Override
    public boolean enqueue (byte[] message)
    {
        Assert.notNull(message, "The message must be not null");
        Assert.isTrue(message.length > 0, "The message must be not empty");
        return queue.offer(message);
    }

    @Override
    public boolean isEmpty() {
        return current == null && queue.isEmpty();
    }
}
