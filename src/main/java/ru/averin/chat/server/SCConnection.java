package ru.averin.chat.server;

import lombok.Getter;
import lombok.Setter;
import ru.averin.chat.server.user.UserDetails;

import java.nio.channels.SocketChannel;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Connection by a {@link SocketChannel}
 */
public class SCConnection implements Connection<SocketChannel> {

    private static final AtomicLong ID = new AtomicLong(0);
    private final long id = ID.incrementAndGet();

    @Getter
    private final SocketChannel channel;
    @Getter
    private final InputQueue inputQueue = new RBCInputQueue();
    @Getter
    private final OutputQueue outputQueue = new WBCOutputQueue();
    @Getter @Setter
    private UserDetails user;

    public SCConnection(SocketChannel socketChannel) {
        this.channel = socketChannel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return id == ((SCConnection) o).id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
