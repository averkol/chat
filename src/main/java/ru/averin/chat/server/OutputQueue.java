package ru.averin.chat.server;

import java.io.IOException;

public interface OutputQueue<T> {
    /**
     * Writes messages from queue.
     * Adds line separator to each message.
     *
     * @param destination the destination of data sending
     * @return the number of bytes written
     * @throws IOException some I/O error occurs
     */
    int drainTo(T destination) throws IOException;

    /**
     * Adds new message to the tail of queue
     *
     * @param message the new message
     * @return true if message was added, else - false
     * @throws IllegalArgumentException if the message is null or empty
     */
    boolean enqueue(byte[] message);

    /**
     * Returns true if queue contains no data for writing
     */
    boolean isEmpty();
}
