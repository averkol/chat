package ru.averin.chat.server;

import ru.averin.chat.server.user.UserDetails;

public interface Connection<T> {
    T getChannel();
    InputQueue<T> getInputQueue();
    OutputQueue<T> getOutputQueue();
    void setUser(UserDetails user);
    UserDetails getUser();
}
