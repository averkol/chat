package ru.averin.chat.server;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.nio.channels.SocketChannel;

@Getter @Setter
@AllArgsConstructor
public class SocketChannelState implements State<SocketChannel> {
    private volatile int readyOps;
}
