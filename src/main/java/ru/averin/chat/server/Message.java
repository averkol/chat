package ru.averin.chat.server;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Message {
    private Connection sender;
    private byte[] content;
}
