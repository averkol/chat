package ru.averin.chat.server;

public interface ConnectionHandler<T, S extends State<T>> extends Runnable {
    Connection<T> getConnection();
    void updateState(S newState);
    boolean isDied();
    void stop();
    boolean isRunning();
}
