package ru.averin.chat.server.component.command;

import java.util.Collection;

public interface CommandRepository {
    void add(Command command);
    Collection<Command> getAll();
    Collection<String> getAllNames();
    Command getByName(String name);
    boolean exists(String name);
}
