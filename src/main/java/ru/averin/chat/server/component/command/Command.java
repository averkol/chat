package ru.averin.chat.server.component.command;

import ru.averin.chat.server.Connection;

public interface Command {
    String execute(Connection sender, String[] args);
    String getName();
    String getDescription();
}
