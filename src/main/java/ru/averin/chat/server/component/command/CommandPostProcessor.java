package ru.averin.chat.server.component.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.hasText;

@Component
@Slf4j
public class CommandPostProcessor implements BeanPostProcessor {
    private final CommandRepository commandRepository;

    @Autowired
    public CommandPostProcessor(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Command) {
            Command command = (Command)bean;
            if (hasText(command.getName())){
                if (commandRepository.exists(command.getName())) {
                    log.warn("Command \"{}\" already has been added - ignore it", command.getName());
                } else {
                    commandRepository.add(command);
                    log.debug("Command \"{}\" has been added", command.getName());
                }
            } else {
                log.warn("Command {} has empty name", beanName);
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
