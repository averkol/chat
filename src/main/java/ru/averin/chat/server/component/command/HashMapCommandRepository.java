package ru.averin.chat.server.component.command;

import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class HashMapCommandRepository implements CommandRepository {

    private final Map<String, Command> commandsMap = new HashMap<>();

    @Override
    public void add(Command command) {
        commandsMap.put(command.getName(), command);
    }

    @Override
    public Collection<Command> getAll() {
        return commandsMap.values();
    }

    @Override
    public Collection<String> getAllNames() {
        return commandsMap.keySet();
    }

    @Override
    public Command getByName(String name) {
        return commandsMap.get(name);
    }

    @Override
    public boolean exists(String name) {
        return commandsMap.containsKey(name);
    }
}
