package ru.averin.chat.server.component;

import ru.averin.chat.server.Connection;

import java.io.IOException;
import java.util.Collection;
import java.util.Queue;

/**
 * Dispatcher of connections.
 * Register new connections, create handlers for them
 */
public interface Dispatcher<T> {

    /**
     * Returns all connections with registered users
     */
    Collection<Connection<T>> getRegisteredUserConnections();

    /**
     *
     * @param acceptedChannels the queue of accepted channels
     * @param allConnections the queue of all client connections
     * @param registeredUserConnections the queue of client connections with registered users
     * @return the thread in which the dispatcher works
     * @throws IOException If an I/O error occurs
     */
    Thread start(Queue<T> acceptedChannels,
                 Queue<Connection<T>> allConnections,
                 Queue<Connection<T>> registeredUserConnections) throws IOException;

    void shutdown();
}
