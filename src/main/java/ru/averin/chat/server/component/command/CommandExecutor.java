package ru.averin.chat.server.component.command;

import ru.averin.chat.server.Connection;

public interface CommandExecutor {
    /**
     * Execute command
     * @param sender a command's sender
     * @param commandString a command line - {command_prefix}{command_name} [arguments]
     * @return a result of command executing
     */
    String executeCommand(Connection sender, String commandString);

    /**
     * Commands' first char - every command should start from this char
     */
    char getCommandPrefix();
}
