package ru.averin.chat.server.component;

import java.io.IOException;
import java.util.Queue;

/**
 * Listen port, accept new connections and put them into the acceptedChannels queue
 */
public interface Acceptor<T> {
    Thread start(String host, int port, Queue<T> acceptedChannels) throws IOException;
    void shutdown();
    boolean isRunning();
}
