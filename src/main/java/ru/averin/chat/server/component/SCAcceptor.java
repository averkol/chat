package ru.averin.chat.server.component;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Queue;

/**
 * Acceptor of {@link SocketChannel} connections
 */
@Component
@Slf4j
public class SCAcceptor implements Acceptor<SocketChannel> {

    private ServerSocketChannel listenSocket;
    @Getter
    private volatile boolean running = false;
    private Queue<SocketChannel> acceptedChannels;

    @Override
    public Thread start(String host, int port, Queue<SocketChannel> acceptedChannels) throws IOException {
        if (running) {
            throw new IllegalStateException("Acceptor is already running");
        }
        Assert.notNull(acceptedChannels, "acceptedChannels must not be null");
        this.acceptedChannels = acceptedChannels;
        this.listenSocket = ServerSocketChannel.open();
        this.listenSocket.configureBlocking(false);
        this.listenSocket.bind(new InetSocketAddress(host, port));
        this.running = true;
        Thread thread = new Thread(this::run, "Acceptor");
        thread.start();
        return thread;
    }

    @Override
    public void shutdown() {
        running = false;
    }

    private void run() {
        while (running) {
            try {
                SocketChannel client = listenSocket.accept();
                if (client != null) {
                    acceptedChannels.offer(client);
                }
            } catch (ClosedByInterruptException e) {
                log.error("ServerSocketChannel closed by interrupt. Exiting..." + e);
                running = false;
                return;
            } catch (ClosedChannelException e) {
                log.error("ServerSocketChannel is closed. Exiting...", e);
                running = false;
                return;

            } catch (Throwable t) {
                log.error(t.getMessage(), t);
                running = false;
                //chanel may not been closed, so not return
            }
        }
        log.debug("Closing socket...");
        try {
            listenSocket.close();
        } catch (IOException e) {
            log.error("Caught an exception shutting down", e);
        }
    }
}
