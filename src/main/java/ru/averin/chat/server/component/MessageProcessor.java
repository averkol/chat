package ru.averin.chat.server.component;

import ru.averin.chat.server.Connection;
import ru.averin.chat.server.Message;

import java.util.Collection;

public interface MessageProcessor {

    /**
     * Start processor
     * @param connections the collection of registered connections
     * @param workerCount the count of workers (every worker runs in separate thread)
     * @param msgCount the count of messages that will be sent to user after connecting
     */
    void start(Collection<Connection> connections, int workerCount, int msgCount);
    /**
     * Messages handling.
     * If connection isn't registered (there isn't user) - try register it.
     * Else if it's command - execute it and send result to
     * sender, else broadcast message to all registered users.
     */
    void handleMessage(Message message);
    void shutdown();
    boolean isRunning();
}
