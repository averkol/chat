package ru.averin.chat.server.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.averin.chat.server.*;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.ExecutorService;

/**
 * Dispatcher of {@link SocketChannel} connections
 */
@Component
@Slf4j
public class SCDispatcher implements Dispatcher<SocketChannel> {

    private final MessageProcessor messageProcessor;
    private final ExecutorService executor;

    private volatile boolean running = false;

    private Selector selector;
    private Queue<SocketChannel> acceptedChannels;
    private Queue<Connection<SocketChannel>> allConnections;
    // connections with registered users - separate collection for fast iterating without filtering
    private Queue<Connection<SocketChannel>> registeredUserConnections;
    private final Map<ConnectionHandler<SocketChannel, SocketChannelState>, SelectionKey> handlerKeys = new HashMap<>();

    @Autowired
    public SCDispatcher(MessageProcessor messageProcessor, ExecutorService executor) {
        this.messageProcessor = messageProcessor;
        this.executor = executor;
    }

    @Override
    public Thread start(Queue<SocketChannel> acceptedChannels,
                        Queue<Connection<SocketChannel>> allConnections,
                        Queue<Connection<SocketChannel>> registeredUserConnections) throws IOException {
        if (this.running) {
            throw new IllegalStateException("Dispatcher is already running");
        }
        Assert.notNull(acceptedChannels, "acceptedChannels must not be null");
        this.acceptedChannels = acceptedChannels;
        Assert.notNull(allConnections, "allConnections must not be null");
        this.allConnections = allConnections;
        Assert.notNull(registeredUserConnections, "registeredUserConnections must not be null");
        this.registeredUserConnections = registeredUserConnections;
        this.selector = Selector.open();
        this.running = true;
        Thread thread = new Thread(this::run, "Dispatcher");
        thread.start();
        return thread;
    }

    @Override
    public void shutdown() {
        this.running = false;
    }

    private void run() {
        while (running){
            registerChannels();
            try {
                selector.select(50);
                Set<SelectionKey> keys = selector.selectedKeys();
                keys.forEach(this::updateReadyOps);
                keys.clear();
            } catch (IOException e) {
                log.error("Unexpected I/O exception", e);
            }
        }
        log.debug("Stopping handlers...");
        handlerKeys.keySet().forEach(ConnectionHandler::stop);
        log.debug("Closing channels...");
        allConnections.forEach(c -> {
            try {
                c.getChannel().close();
            } catch (IOException e) {
                log.error("Unexpected I/O Exception closing channel", e);
            }
        });
        log.debug("Closing the selector...");
        try {
            selector.close();
            log.debug("Selector has been closed");
        } catch (IOException e) {
            log.error("Unexpected I/O Exception closing selector", e);
        }
        log.debug("Shutting down the executor...");
        executor.shutdownNow();
        log.debug("Dispatcher has been shutdown");
    }

    private void registerChannels(){
        SocketChannel channel;
        while ((channel = acceptedChannels.poll()) != null){
            try {
                channel.configureBlocking(false);
                SCConnection connection = new SCConnection(channel);
                SCConnectionHandler connectionHandler = new SCConnectionHandler(connection, messageProcessor);
                selector.wakeup();
                SelectionKey key = channel.register (selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE, connectionHandler);
                handlerKeys.put(connectionHandler, key);
                allConnections.add(connection);
                invokeHandler(key);
                log.debug("New connection. Total: " + allConnections.size());
            } catch (ClosedChannelException e) {
                log.warn("Connection registration error: channel is closed", e);
            } catch (IOException e) {
                log.warn("Connection registration error: IOException", e);
            }
        }
    }

    /**
     * Invoke connection handler from key.attachment()
     */
    private void invokeHandler (SelectionKey key) {
        SCConnectionHandler handler = (SCConnectionHandler) key.attachment();
        handler.updateState(new SocketChannelState(key.readyOps()));
        executor.submit (handler);
    }

    /**
     * Synchronize readyOps of the key and its handler.
     * If a handler is died - remove it and cancel the key
     */
    private void updateReadyOps(SelectionKey key){
        SCConnectionHandler handler = (SCConnectionHandler) key.attachment();
        if (handler.isDied()){
            Connection connection = handler.getConnection();
            handlerKeys.remove(handler);
            allConnections.remove(connection);
            log.debug("Connection closed. Total: " + allConnections.size());
            registeredUserConnections.remove(connection);
            log.info("User disconnect. Total: " + registeredUserConnections.size());
            key.cancel();
            try {
                key.channel().close();
                log.debug("client channel has been closed");
            } catch (IOException e) {
                log.warn("Error on closing channel", e);
            }
            return;
        }
        handler.updateState(new SocketChannelState(key.readyOps()));
    }

    @Override
    public Collection<Connection<SocketChannel>> getRegisteredUserConnections() {
        return this.registeredUserConnections;
    }
}
