package ru.averin.chat.server.component.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.averin.chat.server.Connection;

import java.util.stream.Collectors;

@Component
public class HelpCommand implements Command {

    private final CommandRepository commandRepository;
    private String result = null;

    @Autowired
    public HelpCommand(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public String execute(Connection sender, String[] args) {
        if (result == null) {
            int maxNameLength = commandRepository.getAllNames().stream()
                    .map(String::length)
                    .max(Integer::compareTo).get();
            result = commandRepository.getAll().stream()
                    .map(c -> String.format("%-"+maxNameLength+"s| %s", c.getName(), c.getDescription()))
                    .sorted()
                    .collect(Collectors.joining("\r"));
        }
        return "\r" + result;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Get a list of available commands";
    }
}
