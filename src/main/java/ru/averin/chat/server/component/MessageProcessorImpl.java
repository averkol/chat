package ru.averin.chat.server.component;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.BufferUtils;
import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.averin.chat.server.Connection;
import ru.averin.chat.server.Message;
import ru.averin.chat.server.component.command.CommandExecutor;
import ru.averin.chat.server.user.User;

import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Component
@Slf4j
public class MessageProcessorImpl implements MessageProcessor {

    private final CommandExecutor commandExecutor;
    private final char COMMAND_PREFIX;
    private Collection<Connection> connections;
    @Getter
    private volatile boolean running = false;
    private final Queue<Message> broadcastMessages = new ConcurrentLinkedQueue<>();
    private Buffer lastMessages;
    private int workersCount;

    @Autowired
    public MessageProcessorImpl(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
        this.COMMAND_PREFIX = commandExecutor.getCommandPrefix();
    }

    @Override
    public void start(Collection<Connection> connections, int workersCount, int msgCount) {
        if (running) {
            throw new IllegalStateException("Message processor is already running");
        }
        Assert.notNull(connections, "connections must not be null");
        this.connections = connections;
        Assert.isTrue(workersCount > 0, "workersCount must be more than 0");
        this.workersCount = workersCount;
        Assert.isTrue(msgCount > 0, "msgCount must be more than 0");
        this.lastMessages = BufferUtils.synchronizedBuffer(new CircularFifoBuffer(msgCount));
        this.running = true;
        runWorkers();
    }

    private void runWorkers() {
        for (int i = 0; i < workersCount; i++){
            Thread worker = new Thread(this::broadcast, "Msg worker " + i);
            worker.start();
        }
    }

    @Override
    public void handleMessage(Message message) {
        Connection sender = message.getSender();
        byte[] messageContent = message.getContent();
        boolean isRegistered = sender.getUser() != null;
        if (!isRegistered){
            registerUser(sender, new String(messageContent));
            return;
        }
        if (isCommand(message.getContent())){
            String result = commandExecutor.executeCommand(sender, new String(messageContent));
            sender.getOutputQueue().enqueue(formatServerMessage(result));
        } else {
            broadcastMessages.offer(message);
            // TODO: 11.07.17 need optimizing
            lastMessages.add(new String(formatMessage(message)));
        }
    }

    private void registerUser(Connection connection, String name){
        synchronized (connections) {
            boolean isUniqueName = !connections.stream()
                    .anyMatch(c -> name.equals(c.getUser().getName()));
            if (isUniqueName) {
                connection.setUser(new User(name));
                connections.add(connection);
                String successMessage = String.format("Hi, %s! Get server commands by $help", name);
                connection.getOutputQueue().enqueue(successMessage.getBytes());
                sendLastMessages(connection);
                log.info("New user registered. Total: " + connections.size());
                return;
            }
        }
        String errorMessage = String.format("Name \"%s\" is already taken", name);
        connection.getOutputQueue().enqueue(errorMessage.getBytes());
    }

    private boolean isCommand(byte[] content) {
        // content length must be always greater than 0
        return content[0] == COMMAND_PREFIX;
    }

    private void broadcast(){
        Message message;
        byte[] formattedMessage;
        long count = 0;
        while (running) {
            if ((message = broadcastMessages.poll()) != null) {
                formattedMessage = formatMessage(message);
                for (Connection c : connections) {
                    if (c != message.getSender()) {
                        c.getOutputQueue().enqueue(formattedMessage);
                    }
                }
                if (++count % 1000 == 0){
                    log.debug("I processed {} | in queue {}", count, broadcastMessages.size());
                }
            }
        }
    }

    private void sendLastMessages(Connection connection){
        String message = "Last messages:\r" +
        lastMessages.stream().collect(Collectors.joining("\r")) +
                "\r---";
        connection.getOutputQueue().enqueue(message.getBytes());
    }

    private byte[] formatServerMessage(String text) {
        return ("[SERVER]: " + text).getBytes();
    }

    private byte[] formatMessage(Message message) {
        return ArrayUtils.addAll(
                String.format("[%s]: ", message.getSender().getUser().getName()).getBytes(),
                message.getContent());
    }

    @Override
    public void shutdown() {
        running = false;
    }
}
