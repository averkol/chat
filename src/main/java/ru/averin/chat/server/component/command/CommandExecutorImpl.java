package ru.averin.chat.server.component.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.averin.chat.server.Connection;

import static org.springframework.util.StringUtils.hasText;

@Component
public class CommandExecutorImpl implements CommandExecutor {
    private final static String EMPTY_COMMAND_NAME = "Command's name must be not empty";
    private final static String NOT_FOUND_COMMAND = "Command \"%s\" not found";
    private final static char COMMAND_PREFIX = '$';


    private final HashMapCommandRepository commandRepository;

    @Autowired
    public CommandExecutorImpl(HashMapCommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public char getCommandPrefix() {
        return COMMAND_PREFIX;
    }

    @Override
    public String executeCommand(Connection sender, String commandString){
        int commandNameEnd = commandString.indexOf(" ");
        if (commandNameEnd == -1) {
            commandNameEnd = commandString.length();
        }
        String commandName = commandString.substring(1, commandNameEnd);
        if (!hasText(commandName)){
            return EMPTY_COMMAND_NAME;
        }
        Command command = commandRepository.getByName(commandName);
        if (command == null){
            return String.format(NOT_FOUND_COMMAND, commandName);
        }
        String[] commandArgs = commandString.substring(commandNameEnd).trim().split(" ");
        return command.execute(sender, commandArgs);
    }
}
