package ru.averin.chat.server.component.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.averin.chat.server.Connection;
import ru.averin.chat.server.component.Dispatcher;

import static org.springframework.util.StringUtils.hasText;

@Component
@Slf4j
public class ChangeNameCommand implements Command {

    private final Dispatcher<?> dispatcher;

    @Autowired
    public ChangeNameCommand(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public String execute(Connection sender, String[] args) {
        String newName;
        if (args == null || args.length == 0 || !hasText((newName = args[0].trim()))) {
            return "Enter your name. Format: change-name <new_name>";
        }
        if (sender.getUser().getName().equals(newName)){
            return "New name must be different from the your current name";
        }
        return renameUser(sender, args[0].trim());
    }

    private String renameUser(Connection connection, String name){
        synchronized (dispatcher.getRegisteredUserConnections()) {
            boolean isUniqueName = !dispatcher.getRegisteredUserConnections().stream()
                    .anyMatch(c -> c != connection && name.equals(c.getUser().getName()));
            if (isUniqueName) {
                String oldName = connection.getUser().getName();
                connection.getUser().setName(name);
                log.debug("User {} has changed name to {}", oldName, name);
                return "Your name has been changed";
            }
        }
        return String.format("Name \"%s\" is already taken", name);
    }

    @Override
    public String getName() {
        return "change-name";
    }

    @Override
    public String getDescription() {
        return "Change your name. Format: change-name <new_name>";
    }
}
