package ru.averin.chat.server.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.averin.chat.server.Connection;
import ru.averin.chat.server.config.ServerConfig;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component
public class Server {
    private final Acceptor<SocketChannel> acceptor;
    private final Dispatcher<SocketChannel> dispatcher;
    private final MessageProcessor messageProcessor;
    private final ServerConfig serverConfig;

    private final Queue<SocketChannel> acceptedChannels = new ConcurrentLinkedQueue<>();
    private final Queue<Connection<SocketChannel>> allConnections = new ConcurrentLinkedQueue<>();
    // connections with registered users - separate collection for fast iterating without filtering
    private final Queue<Connection<SocketChannel>> registeredUserConnections = new ConcurrentLinkedQueue<>();

    @Autowired
    public Server(Acceptor<SocketChannel> acceptor,
                  Dispatcher<SocketChannel> dispatcher,
                  MessageProcessor messageProcessor,
                  ServerConfig serverConfig) {
        this.acceptor = acceptor;
        this.dispatcher = dispatcher;
        this.messageProcessor = messageProcessor;
        this.serverConfig = serverConfig;
    }

    public void run(String host, int port, int workersCount) throws IOException {
        messageProcessor.start((Collection) registeredUserConnections, workersCount, serverConfig.getMessagesCount());
        dispatcher.start(acceptedChannels, allConnections, registeredUserConnections);
        acceptor.start(host, port, acceptedChannels);
    }

    public void stop() {
        acceptor.shutdown();
        dispatcher.shutdown();
        messageProcessor.shutdown();
    }
}
