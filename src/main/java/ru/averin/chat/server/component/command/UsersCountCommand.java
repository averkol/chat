package ru.averin.chat.server.component.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.averin.chat.server.Connection;
import ru.averin.chat.server.component.Dispatcher;

@Component
public class UsersCountCommand implements Command {

    private final Dispatcher dispatcher;

    @Autowired
    public UsersCountCommand(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public String execute(Connection sender, String[] args) {
        return String.valueOf(dispatcher.getRegisteredUserConnections().size());
    }

    @Override
    public String getName() {
        return "users-count";
    }

    @Override
    public String getDescription() {
        return "Get count of registered users";
    }
}
