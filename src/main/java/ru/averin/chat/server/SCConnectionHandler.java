package ru.averin.chat.server;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.averin.chat.server.component.MessageProcessor;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Handler of a {@link SocketChannel} connection
 */
@Slf4j
public class SCConnectionHandler implements ConnectionHandler<SocketChannel, SocketChannelState> {

    private static final AtomicLong ID = new AtomicLong(0);
    private final long id = ID.incrementAndGet();
    private final static byte[] HELLO_MESSAGE = "Enter your name".getBytes();
    @Getter
    private final Connection<SocketChannel> connection;
    private final MessageProcessor messageProcessor;
    @Getter
    private volatile boolean running;
    private volatile int readyOps;
    @Getter
    private volatile boolean died;

    public SCConnectionHandler(Connection<SocketChannel> connection,
                               MessageProcessor messageProcessor) {
        this.connection = connection;
        this.messageProcessor = messageProcessor;
        this.connection.getOutputQueue().enqueue(HELLO_MESSAGE);
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                writeToChannel();
                readFromChannel();
                handleMessages();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // may "normally" occurs when dispatcher is shutting down -
                // then running must be false
                if (running) {
                    log.warn("InterruptedException has occurred, but handler is not stopped. Handler is stopping", e);
                }
                died = true;
                running = false;
            } catch (IOException e) {
                // "normal" - expected exceptions
                // usually it occurs after client has closed connection
                died = true;
                running = false;
                log.debug("IOException has occurred. Handler is stopping");
            } catch (Throwable e) {
                died = true;
                running = false;
                log.error("Unexpected exception. Handler is stopping", e);
            }
        }
    }

    @Override
    public void stop() {
        this.running = false;
        log.debug("Handler is stopping");
    }

    @Override
    public void updateState(SocketChannelState newState) {
        this.readyOps = newState.getReadyOps();
    }

    /**
     * Write messages to the channel
     */
    private void writeToChannel() throws IOException {
        if ((this.readyOps & SelectionKey.OP_WRITE) != 0 && !connection.getOutputQueue().isEmpty()){
            connection.getOutputQueue().drainTo(connection.getChannel());
        }
    }

    /**
     * Fill the input queue from the channel
     */
    private void readFromChannel() throws IOException {
        if ((this.readyOps & SelectionKey.OP_READ) != 0) {
            int bytes = connection.getInputQueue().fillFrom(connection.getChannel());
            // bytes = -1 if the channel has reached end-of-stream
            if (bytes == -1) {
                died = true;
                running = false;
            }
        }
    }

    /**
     * Handle input messages
     */
    private void handleMessages() {
        byte[] message;
        while ((message = connection.getInputQueue().getNextMessage()) != null) {
            if (message.length == 0){
                log.error("message.length = 0");
            }
            messageProcessor.handleMessage(new Message(connection, message));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return !(o == null || getClass() != o.getClass())
                && this.id == ((SCConnectionHandler) o).id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
