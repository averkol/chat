package ru.averin.chat;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.averin.chat.client.Client;
import ru.averin.chat.client.bot.Bot;
import ru.averin.chat.server.component.Server;
import ru.averin.chat.server.config.ServerConfig;

import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication
@Slf4j
@EnableScheduling
public class Application implements CommandLineRunner {

    @Autowired
    private Server server;
    @Autowired
    private ServerConfig serverConfig;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String[] args) throws Exception {
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            String mode = cmd.getOptionValue("m", "server");
            String host = cmd.getOptionValue("h", serverConfig.getHost());
            int port = cmd.getOptionValue("p") != null ? Integer.valueOf(cmd.getOptionValue("p")): serverConfig.getPort();
            int workersCount = cmd.getOptionValue("w") != null ? Integer.valueOf(cmd.getOptionValue("w")): serverConfig.getWorkersCount();
            int botsCount = cmd.getOptionValue("b") != null ? Integer.valueOf(cmd.getOptionValue("b")): serverConfig.getBotsCount();
            run(mode, host, port, workersCount, botsCount);
        } catch (ParseException | NumberFormatException e) {
            formatter.printHelp("utility-name", options);
            System.exit(1);
            return;
        }
    }

    private void run(String mode, String host, int port, int workersCount, int botsCount) throws IOException {
        if (mode.equals("server")) {
            runServer(host, port, workersCount);
        } else if (mode.equals("client")) {
            runClient(host, port);
        } else if (mode.equals("bots")) {
            runBots(host, port, botsCount);
        } else {
            log.warn("Input mode: server, client or bots");
        }
    }

    private static Options getOptions() {
        Options options = new Options();
        Option modeOption = new Option("m", "mode", true, "app mode: server, client or bots");
        options.addOption(modeOption);
        Option hostOption = new Option("h", "host", true, "hostname or ip-address");
        options.addOption(hostOption);
        Option portOption = new Option("p", "port", true, "port");
        options.addOption(portOption);
        Option botsCountOption = new Option("b", "bots-count", true, "a count of bots");
        options.addOption(botsCountOption);
        Option workersCountOption = new Option("w", "workers-count", true, "a count of workers");
        options.addOption(workersCountOption);
        return options;
    }

    private void runServer(String host, int port, int workersCount) throws IOException {
        log.info("server starting...");
        server.run(host, port, workersCount);
        log.info("server started");
        final Scanner scan = new Scanner(System.in);
        String str;
        while (true){
            str = scan.nextLine();
            if ("stop".equals(str)){
                server.stop();
                return;
            }
        }
    }

    private void runClient(String host, int port){
        Client.runClient(host, port);
    }

    private void runBots(String host, int port, int botsCount) {
        Bot.runBots(host, port, botsCount);
    }

//    @Scheduled(fixedDelay = 5000L)
//    private void showJvmMemory(){
//        long total = Runtime.getRuntime().totalMemory()/1024/1024;
//        long free = Runtime.getRuntime().freeMemory()/1024/1024;
//        long max = Runtime.getRuntime().maxMemory()/1024/1024;
//        log.info("memory: {} | {} | {}", total, free, max);
//    }
}
