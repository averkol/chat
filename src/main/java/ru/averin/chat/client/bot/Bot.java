package ru.averin.chat.client.bot;

import ru.averin.chat.client.Client;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Bot implements Runnable {
		
	private final String host;
	private final int port;
	private final int randomSeed;
	private final static String[] commands = new String[]{"$help ", "$change-name ", "$users-count "};
	
	public Bot(String host, int port, int randomSeed) {		
		this.host = host;
		this.port = port;
		this.randomSeed = randomSeed;
	}

	@Override
	public void run() {
		final RandomString randomString = new RandomString();
		final Random botRandom = new Random(randomSeed);
		final AtomicInteger count = new AtomicInteger(botRandom.nextInt(100)); // for random sending commands
		new Client(host, port, () -> {
            String str = randomString.nextString(botRandom.nextInt(30)+1);//length of messages: 1-30 chars
            int randomSleep = botRandom.nextInt(9*1000+1)+1000;//1-10 sec between messages
			try {
                Thread.sleep(randomSleep);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            // every 100 messages will send command
            if (count.incrementAndGet() % 100 == 0) {
			    count.set(0);
			    return commands[botRandom.nextInt(commands.length)] + str;
            }
            return str;
        }, text -> {});
	}

	public static void runBots(String host, int port, int botsCount) {
		final Random random = new Random(System.currentTimeMillis());
		for (int i = 0; i < botsCount; i++) {
			(new Thread(new Bot(host, port, random.nextInt()), "bot_"+i)).start();
			// wait 1 sec between creating every 100 bots
			if (i % 100 == 0) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
                    throw new RuntimeException(e);
				}
			}
		}
	}
}
