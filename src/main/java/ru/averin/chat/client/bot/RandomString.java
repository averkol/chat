package ru.averin.chat.client.bot;

import org.springframework.util.Assert;

import java.util.Random;

public class RandomString {

	private static final char[] SYMBOLS;

	static {
		StringBuilder tmp = new StringBuilder();
		for (char ch = '0'; ch <= '9'; ++ch)
			tmp.append(ch);
		for (char ch = 'a'; ch <= 'z'; ++ch)
			tmp.append(ch);
		SYMBOLS = tmp.toString().toCharArray();
	}

	private final Random random = new Random();

	public String nextString(int length) {
        Assert.isTrue(length > 0, "length must be greater then 0");
		char[] buf = new char[length];
		for (int i = 0; i < length; i++) {
			buf[i] = SYMBOLS[random.nextInt(SYMBOLS.length)];
		}
		return new String(buf);
	}
}
