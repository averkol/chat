package ru.averin.chat.client;

public interface MessageInput {
	String readLine() throws Exception;
}
