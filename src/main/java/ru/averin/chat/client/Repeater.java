package ru.averin.chat.client;

/**
 * Read data from the {@link MessageInput} and write it to the {@link MessageOutput}
 */
public interface Repeater {
    /**
     * Start reading data from the input and writing to the output
     * @param input the data input
     * @param output the data output
     * @return the thread in which the method is started
     */
    Thread start(MessageInput input, MessageOutput output);

    /**
     * Stop repeating. Second call doesn't have any effect.
     */
    void stop();
}
