package ru.averin.chat.client;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import static org.springframework.util.StringUtils.hasText;

/**
 * Simple client.
 * Creates a stream socket and connects it to the specified port number on the named host. 
 * Read data from the standard input stream (System.in) and write it to an output stream for socket.
 * Read data from the input stream for socket and write it to the standard output stream (System.out).
 */
@Slf4j
public class Client {
	private Socket socket;
	private BufferedReader socketInput;
	private PrintWriter socketOutput;	
	private Repeater repeater = new RepeaterImpl();
	/**
	 * Constructor
	 * @param host then hostname or IP address of server
	 * @param port the port number
	 */
	public Client(String host, int port, final MessageInput messageInput, final MessageOutput messageOutput) {
		try {
			socket = new Socket(host, port);
			socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			socketOutput = new PrintWriter(socket.getOutputStream(), true);
            repeater.start(socketInput::readLine, messageOutput::printLine);
			String str;
			while (true) {
				str = messageInput.readLine();
				if (str.equals("exit")) {
					break;
				}
				if (hasText(str)) {
                    socketOutput.println(str);
                }
			}
			repeater.stop();
		}
		catch (UnknownHostException ue) {
			log.error(String.format("Host or IP address %s could not be determined", host), ue);
		}
		catch (Exception e) {
		    log.error("Unexpected exception", e);
		} finally {
			close();			
		}
	}

	public static void runClient(String host, int port){
        log.info("Client starting...");
        final Scanner scan = new Scanner(System.in);
        new Client(host, port, scan::nextLine, System.out::println);
        scan.close();
		log.info("Client finished");
    }

	private void close() {
		try {
		    log.info("Closing resources...");
            repeater.stop();
            log.debug("repeater has been stopped");
            if (socket != null) {
                socket.shutdownInput();
                log.debug("socket's input has been shutdown");
            }
            if (socketInput != null) {
                socketInput.close();
                log.debug("socket's reader has been closed");
            }
            if (socketOutput != null) {
                socketOutput.close();
                log.debug("socket's writer been closed");
            }
            if (socket != null) {
                socket.close();
                log.debug("socket has been closed");
            }
			log.info("Resources have been closed");
		} catch (Exception e) {
			log.error("Exception on closing", e);
		}
	}
}
