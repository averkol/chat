package ru.averin.chat.client.exception;

import ru.averin.chat.client.Repeater;

/**
 * Throws in {@link Repeater}
 */
public class RepeaterException extends RuntimeException {

    public RepeaterException(String message, Throwable cause) {
        super(message, cause);
    }
}
