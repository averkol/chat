package ru.averin.chat.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import ru.averin.chat.client.exception.RepeaterException;

@Slf4j
public class RepeaterImpl implements Repeater {

	private volatile boolean running = false;
	private MessageInput input;
	private MessageOutput output;

	@Override
	public Thread start(MessageInput input, MessageOutput output) {
        Assert.notNull(input, "input must not be null");
        Assert.notNull(output, "output must not be null");
        this.input = input;
		this.output = output;
		this.running = true;
		Thread thread = new Thread(this::repeat, "Repeater");
        thread.start();
		return thread;
	}

	@Override
	public void stop() {
        running = false;
	}

	/**
	 * Read data from input source and write it to output source
	 */
	private void repeat() {
	    while (running) {
            try {
                String str = input.readLine();
                if (str != null) {
                    output.printLine(str);
                }
            } catch (Exception e) {
                this.running = false;
                log.debug("Exception occurs. Repeater is stopped");
                throw new RepeaterException("Exception occurs. Repeater is stopped", e);
            }
        }
	}
	
}
