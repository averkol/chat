package ru.averin.chat.bot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import ru.averin.chat.client.bot.RandomString;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class RandomStringTest {
    private RandomString randomString = new RandomString();

    @Test
    public void testNextString(){
        check(1);
        check(10);
        check(100);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testNegativeLength(){
        check(-1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testZeroLength(){
        check(0);
    }

    private void check(int length){
        assertEquals(randomString.nextString(length).length(), length);
    }
}
