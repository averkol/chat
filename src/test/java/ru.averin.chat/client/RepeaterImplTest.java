package ru.averin.chat.client;

import net.jodah.concurrentunit.Waiter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeoutException;

import static java.lang.Thread.State.RUNNABLE;
import static java.lang.Thread.State.TERMINATED;

@RunWith(SpringRunner.class)
public class RepeaterImplTest {
    private final Repeater repeater = new RepeaterImpl();
    private final Waiter waiter = new Waiter();
    private Thread repeaterThread;
    private volatile String input = "";
    private volatile String output;

    @Before
    public void startRepeater() {
        repeaterThread = repeater.start(() -> input,
                text -> {
                    output = text;
                    waiter.resume();
                });
    }

    @Test
    public void testRepeat() throws TimeoutException {
        check("test");
        check("");
    }

    private void check(String message) throws TimeoutException {
        input = message;
        waiter.await(100);
        Assert.assertEquals(input, output);
    }

    @After
    public void stopRepeater() throws InterruptedException {
        Assert.assertEquals(repeaterThread.getState(), RUNNABLE);
        repeater.stop();
        Thread.sleep(100); //wait while repeater stops
        Assert.assertEquals(repeaterThread.getState(), TERMINATED);
    }
}
