package ru.averin.chat.server;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

@RunWith(SpringRunner.class)
public class WBCOutputQueueTest {
    private final WBCOutputQueue outputQueue = new WBCOutputQueue();
    private final String FIRST_MESSAGE = "first";
    private final String SECOND_MESSAGE = "second";

    @Mock
    private WritableByteChannel channel;
    private final ByteBuffer buffer = ByteBuffer.allocate(64);

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
        //mock writing message to channel - will save this message in the buffer
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ByteBuffer arg = (ByteBuffer) args[0];
            this.buffer.put(arg);
            return arg.limit();
        }).when(this.channel).write(any());
    }

    @Test
    public void testAll() throws IOException {
        Assert.assertTrue(outputQueue.enqueue(FIRST_MESSAGE.getBytes()));
        Assert.assertFalse(outputQueue.isEmpty());
        int bytes = outputQueue.drainTo(channel);
        Assert.assertEquals(bytes, FIRST_MESSAGE.length()+1);// + LF
        Assert.assertTrue(Arrays.equals(ArrayUtils.subarray(buffer.array(), 0, buffer.position()), (FIRST_MESSAGE+"\n").getBytes()));
        Assert.assertTrue(outputQueue.isEmpty());
        this.buffer.clear();

        Assert.assertTrue(outputQueue.enqueue(SECOND_MESSAGE.getBytes()));
        Assert.assertFalse(outputQueue.isEmpty());
        bytes = outputQueue.drainTo(channel);
        Assert.assertEquals(bytes, SECOND_MESSAGE.length()+1);// + LF
        Assert.assertTrue(Arrays.equals(ArrayUtils.subarray(buffer.array(), 0, buffer.position()), (SECOND_MESSAGE+"\n").getBytes()));
        Assert.assertTrue(outputQueue.isEmpty());
    }
}
