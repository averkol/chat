package ru.averin.chat.server;

import net.jodah.concurrentunit.Waiter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import ru.averin.chat.server.component.SCAcceptor;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.State.RUNNABLE;
import static java.lang.Thread.State.TERMINATED;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class SCAcceptorTest {
    private final String host = "localhost";
    private final int port = 9998;
    private final SCAcceptor acceptor = new SCAcceptor();
    @Mock
    private Queue<SocketChannel> acceptedChannels;
    private Thread acceptorThread;
    private final Waiter waiter = new Waiter();

    @Before
    public void start() throws IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.when(acceptedChannels.offer(any(SocketChannel.class)))
                .then(invocation -> {
                    Object[] args = invocation.getArguments();
                    SocketChannel channel = (SocketChannel) args[0];
                    waiter.assertNotNull(channel);
                    waiter.resume();
                    return true;
                });
        acceptorThread = acceptor.start(host, port, acceptedChannels);
    }

    @Test
    public void testAccept() throws IOException, TimeoutException {
        // check that the acceptor accepts new connection and transmit it into register method
        Socket socket = new Socket(host, port);
        waiter.await(100); //wait while acceptor is working
        socket.close();
    }

    @After
    public void stop() throws InterruptedException {
        Assert.assertEquals(acceptorThread.getState(), RUNNABLE);
        acceptor.shutdown();
        Thread.sleep(100); //wait while acceptor is stopping
        Assert.assertEquals(acceptorThread.getState(), TERMINATED);
    }
}
