package ru.averin.chat.server;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

@RunWith(SpringRunner.class)
public class RBCInputQueueTest {
    private RBCInputQueue inputQueue = new RBCInputQueue();
    @Mock
    private ReadableByteChannel channel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFillFrom() throws IOException {
        prepareChannel(asList("first\n"));
        int size = inputQueue.fillFrom(channel);
        Assert.assertEquals(size, 6);
    }

    @Test
    public void testGetNextMessage() throws IOException {
        prepareChannel(asList("first\n"));
        inputQueue.fillFrom(channel);
        byte[] message = inputQueue.getNextMessage();
        Assert.assertEquals(new String(message), "first");
        message = inputQueue.getNextMessage();
        Assert.assertEquals(message, null);
    }

    @Test
    public void testGetNextMessages() throws IOException {
        prepareChannel(asList("first\nseco", "nd\nt", "hird\n"));
        inputQueue.fillFrom(channel);
        byte[] message = inputQueue.getNextMessage();
        Assert.assertEquals(new String(message), "first");
        message = inputQueue.getNextMessage();
        Assert.assertEquals(new String(message), "second");
        message = inputQueue.getNextMessage();
        Assert.assertEquals(new String(message), "third");
        message = inputQueue.getNextMessage();
        Assert.assertEquals(message, null);
    }

    @Test
    public void testGetNextTwoMessagesInOne() throws IOException {
        prepareChannel(asList("first\nsecond\n"));
        inputQueue.fillFrom(channel);
        byte[] message = inputQueue.getNextMessage();
        Assert.assertEquals(new String(message), "first");
        message = inputQueue.getNextMessage();
        Assert.assertEquals(new String(message), "second");
        message = inputQueue.getNextMessage();
        Assert.assertEquals(message, null);
    }

    /**
     * put messages into the buffer of channel.read method
     */
    private void prepareChannel(List<String> messages) throws IOException {
        AtomicInteger i = new AtomicInteger(0);
        // mock reading from channel - will put messages into the method buffer
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ByteBuffer methodBuffer = (ByteBuffer) args[0];
            if (i.get() < messages.size()) {
                ByteBuffer buffer = ByteBuffer.wrap(messages.get(i.getAndIncrement()).getBytes());
                methodBuffer.put(buffer);
                return buffer.limit();
            }
            return 0;
        }).when(channel).read(any());
    }
}
