package ru.averin.chat.server;

import net.jodah.concurrentunit.Waiter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import ru.averin.chat.server.component.MessageProcessor;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.State.RUNNABLE;
import static java.lang.Thread.State.TERMINATED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;

@RunWith(SpringRunner.class)
public class SCConnectionHandlerTest {
    @Mock private Connection<SocketChannel> connection;
    @Mock private InputQueue<SocketChannel> inputQueue;
    @Mock private OutputQueue<SocketChannel> outputQueue;
    @Mock private SocketChannel channel;
    @Mock private MessageProcessor messageProcessor;
    private volatile byte[] testData = "test\n".getBytes();
    private volatile byte[] channelData = null;
    private volatile byte[] inputQueueData = null;
    private volatile byte[] outputQueueData = null;
    private volatile byte[] messageProcessorData = null;
    private SCConnectionHandler handler;
    private final Waiter waiter = new Waiter();
    private Thread handlerThread = null;

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.when(connection.getChannel()).thenReturn(channel);
        //mock input queue
        Mockito.when(connection.getInputQueue()).thenReturn(inputQueue);
        Mockito.when(inputQueue.fillFrom(channel))
                .then(invocationOnMock -> {
                    inputQueueData = channelData;
                    channelData = null;
                    waiter.resume();
                    return inputQueueData.length;
                });
        Mockito.when(inputQueue.getNextMessage())
                .then(invocation -> {
                    byte[] result = inputQueueData;
                    inputQueueData = null;
                    waiter.resume();
                    return result;
                });
        // mock output queue
        Mockito.when(connection.getOutputQueue()).thenReturn(outputQueue);
        Mockito.when(outputQueue.drainTo(channel))
                .then(invocation -> {
                    channelData = outputQueueData;
                    outputQueueData = null;
                    waiter.assertTrue(Arrays.equals(testData, channelData));
                    waiter.resume();
                    return channelData.length;
                });
        // mock messageProcessor
        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            Message message = (Message) args[0];
            Assert.assertEquals(message.getSender(), connection);
            messageProcessorData = Arrays.copyOf(message.getContent(), message.getContent().length);
            waiter.assertTrue(Arrays.equals(testData, messageProcessorData));
            waiter.resume();
            return 0;
        }).when(messageProcessor).handleMessage(any());

        handler = new SCConnectionHandler(connection, messageProcessor);
    }

    @Test
    public void testReading() throws TimeoutException {
        //check that data from the channel will be in the messageProcessor
        channelData = testData;
        handler.updateState(new SocketChannelState(SelectionKey.OP_READ));
        handlerThread = new Thread(handler);
        handlerThread.start();
        waiter.await(100, 2); //wait while the handler is working
    }

    @Test
    public void testWriting() throws TimeoutException {
        //check that data from the output queue will be in the channel
        outputQueueData = testData;
        handler.updateState(new SocketChannelState(SelectionKey.OP_WRITE));
        handlerThread = new Thread(handler);
        handlerThread.start();
        waiter.await(100, 1); //wait while the handler is working
    }

    @After
    public void stop() throws InterruptedException {
        Assert.assertEquals(handlerThread.getState(), RUNNABLE);
        handler.stop();
        // wait while the handler is stopping (more than usual 100 ms because
        // of the handler may be sleeping
        Thread.sleep(300);
        Assert.assertEquals(handlerThread.getState(), TERMINATED);
    }
}
