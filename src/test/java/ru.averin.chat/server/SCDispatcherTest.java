package ru.averin.chat.server;

import net.jodah.concurrentunit.Waiter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import ru.averin.chat.server.component.MessageProcessor;
import ru.averin.chat.server.component.SCDispatcher;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.State.RUNNABLE;
import static java.lang.Thread.State.TERMINATED;
import static org.mockito.ArgumentMatchers.*;

@RunWith(SpringRunner.class)
public class SCDispatcherTest {

    private SCDispatcher dispatcher;
    private final Queue<SocketChannel> acceptedChannels = new ConcurrentLinkedQueue<>();
    private final Queue<Connection<SocketChannel>> allConnections = new ConcurrentLinkedQueue<>();
    private final Queue<Connection<SocketChannel>> registeredUserConnections = new ConcurrentLinkedQueue<>();

    @Mock private MessageProcessor messageProcessor;
    @Mock private ExecutorService executorService;
    @Mock private SocketChannel channel;
    @Mock private SelectionKey key;

    private Thread dispatcherThread;
    private final Waiter waiter = new Waiter();

    @Before
    public void init() throws IOException {
        MockitoAnnotations.initMocks(this);
        Mockito.when(channel.configureBlocking(anyBoolean()))
                .then(invocation -> channel);
        Mockito.when(channel.register(any(Selector.class), anyInt(), any(ConnectionHandler.class)))
                .then(invocation -> {
                    Object[] args = invocation.getArguments();
                    ConnectionHandler arg = (ConnectionHandler) args[2];
                    Mockito.when(key.attachment()).then(inv -> arg);
                    waiter.resume();
                    return key;
                });
        Mockito.when(executorService.submit(ArgumentMatchers.<Runnable>any()))
                .then(invocation -> {
                    Object[] args = invocation.getArguments();
                    ConnectionHandler arg = (ConnectionHandler) args[0];
                    waiter.assertNotNull(arg);
                    waiter.assertEquals(arg.getConnection().getChannel(), channel);
                    waiter.resume();
                    return null;
                });

        dispatcher = new SCDispatcher(messageProcessor, executorService);
        dispatcherThread = dispatcher.start(acceptedChannels, allConnections, registeredUserConnections);
    }

    @Test
    public void testRegister() throws TimeoutException {
        // check that the dispatcher register new connection and invoke handler for it
        acceptedChannels.offer(channel);
        waiter.await(100, 2); //wait while the dispatcher is working
    }

    @After
    public void stop() throws InterruptedException {
        Assert.assertEquals(dispatcherThread.getState(), RUNNABLE);
        dispatcher.shutdown();
        Thread.sleep(100); //wait while the dispatcher is stopping
        Assert.assertEquals(dispatcherThread.getState(), TERMINATED);
    }
}
